package com.trayio.task.routes.workflows

import akka.http.scaladsl.server.Route
import com.candied.WrakkaRoute
import com.candied.json.SprayJson

trait WorkflowsRoute extends WrakkaRoute with SprayJson with WorkflowsRepository {
  lazy val workflowsRoute: Route =
    pathPrefix("workflows") {
      get {
        complete(getWorkflows)
      }
      path(Segment) { workflowId =>
        get {
          complete(getWorkflowById(workflowId))
        }
        delete {
          complete(deleteWorkflowById(workflowId))
        }
      }
    }
}
