package com.trayio.task.routes.workflows

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.pattern.ask
import com.candied.WrakkaContext
import com.trayio.task.actors.SpoofDbActor.{DeleteRecordById, GetRecordById, GetRecords}
import spray.json.DefaultJsonProtocol

object WorkflowsRepository {
  case class Workflow()

  object Workflow extends DefaultJsonProtocol {
    implicit val workflowFormat = jsonFormat0(Workflow.apply)
  }
}

trait WorkflowsRepository extends WrakkaContext {
  import WorkflowsRepository._

  /**
    * Retrieve a list of Workflows.
    *
    * @return A list of Workflows
    */
  def getWorkflows: WrakkaResult[List[Workflow]] =
    WrakkaResult {
      (workflowsStore ? GetRecords).mapTo[List[Workflow]].map(Right(_))
    }

  /**
    * Retrieve a Workflow by its ID, if it exists.
    *
    * @param workflowId The ID of the Workflow to retrieve
    * @return A Workflow, if found
    */
  def getWorkflowById(workflowId: String): WrakkaResult[Workflow] =
    WrakkaResult {
      (workflowsStore ? GetRecordById(workflowId)).mapTo[Option[Workflow]].map {
        case Some(workflow) => Right(workflow)
        case None => Left(StatusCodes.NotFound)
      }
    }

  /**
    * Delete a Workflow by its ID, if it exists.
    *
    * @param workflowId The ID of the Workflow to delete
    * @return Nothing
    */
  def deleteWorkflowById(workflowId: String): WrakkaResult[Unit] =
    WrakkaResult {
      (workflowsStore ? DeleteRecordById(workflowId)).mapTo[Unit].map(Right(_))
    }

  val workflowsStore: ActorRef
}
