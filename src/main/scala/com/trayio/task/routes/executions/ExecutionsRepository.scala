package com.trayio.task.routes.executions

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.pattern.ask
import com.candied.WrakkaContext
import com.trayio.task.actors.SpoofDbActor.{GetRecordById, GetRecords}
import spray.json.DefaultJsonProtocol

object ExecutionsRepository {
  case class Execution(executionId: String)

  object Execution extends DefaultJsonProtocol {
    implicit val executionFormat = jsonFormat1(Execution.apply)
  }
}

trait ExecutionsRepository extends WrakkaContext {
  import ExecutionsRepository._

  /**
    * Retrieve a list of Executions.
    *
    * @return A list of Executions
    */
  def getExecutions: WrakkaResult[List[Execution]] =
    WrakkaResult {
      (executionsStore ? GetRecords).mapTo[List[Execution]].map(Right(_))
    }

  /**
    * Retrieve an Execution by its ID, if it exists.
    *
    * @param executionId The ID of the Execution to retrieve
    * @return An Exececution, if found
    */
  def getExecutionById(executionId: String): WrakkaResult[Execution] =
    WrakkaResult {
      (executionsStore ? GetRecordById(executionId)).mapTo[Option[Execution]].map {
        case Some(execution) => Right(execution)
        case None => Left(StatusCodes.NotFound)
      }
    }

  val executionsStore: ActorRef
}
