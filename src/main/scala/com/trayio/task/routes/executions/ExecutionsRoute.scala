package com.trayio.task.routes.executions

import akka.http.scaladsl.server.Route
import com.candied.WrakkaRoute
import com.candied.json.SprayJson

trait ExecutionsRoute extends WrakkaRoute with SprayJson with ExecutionsRepository {
  lazy val executionsRoute: Route =
    pathPrefix("executions") {
      get {
        complete(getExecutions)
      }
      path(Segment) { executionId =>
        get {
          complete(getExecutionById(executionId))
        }
      }
    }
}
