package com.trayio.task.actors

import akka.actor.{Actor, Props}
import com.candied.SpoofDb
import com.candied.SpoofDb.Record

object SpoofDbActor {
  final case object GetRecords
  final case class GetRecordById(recordId: String)
  final case class CreateRecord(record: Record)
  final case class DeleteRecordById(recordId: String)

  def props: Props = Props[SpoofDbActor]
}

class SpoofDbActor extends SpoofDb[Record] with Actor {
  import SpoofDbActor._

  def receive: Receive = {
    case GetRecords =>
      sender() ! getRecords
    case GetRecordById(recordId) =>
      sender() ! getRecord(recordId)
    case CreateRecord(record) =>
      sender() ! createRecord(record)
    case DeleteRecordById(recordId) =>
      sender() ! deleteRecord(recordId)
  }
}
