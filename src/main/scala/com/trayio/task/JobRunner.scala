package com.trayio.task

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.trayio.task.actors.SpoofDbActor
import com.trayio.task.routes.executions.ExecutionsRoute
import com.trayio.task.routes.workflows.WorkflowsRoute

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

object JobRunner extends App with ExecutionsRoute with WorkflowsRoute {
  implicit val system: ActorSystem = ActorSystem("jobRunnerServer")
  implicit val timeout: Timeout = Timeout(5.seconds)
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher

  val executionsStore = system.actorOf(SpoofDbActor.props, "executionsStoreActor")
  val workflowsStore = system.actorOf(SpoofDbActor.props, "workflowsStoreActor")

  lazy val routes: Route = executionsRoute ~ workflowsRoute

  Http().bindAndHandle(routes, "localhost", 8080)

  println(s"JobRunner online at http://localhost:8080/")

  Await.result(system.whenTerminated, Duration.Inf)
}
