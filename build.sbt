name := "trayio-task"
version := "1.0.0"
scalaVersion := "2.12.6"

lazy val akkaVersion = "2.5.14"
lazy val akkaHttpVersion = "10.1.3"

resolvers += Resolver.bintrayRepo("iainreid693", "maven")

libraryDependencies ++= Seq(
  "com.candied"       %% "spoofdb"              % "0.0.1",
  "com.candied"       %% "wrakka"               % "0.0.3",
  "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream"          % akkaVersion,

  "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
  "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
  "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
  "org.scalatest"     %% "scalatest"            % "3.0.5"         % Test
)
